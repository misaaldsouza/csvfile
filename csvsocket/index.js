var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
/*var fs = require('fs');
var csv = require('fast-csv');

fs.createReadStream('my.csv')
   .pipe(csv())
   .on('data',function(data){
    console.log(data);
   })
   .on('end',function(data){
    console.log('Read finished');
   });

*/



app.get('/', function(req, res){
  res.sendFile(__dirname + 'public');
});
io.on('connection', function(socket){
  console.log('a user connected');
  socket.on('disconnect',function(){
    console.log('user disconnected');
  });
});
io.on('connection', function(socket){
  socket.on('chat message',function(header){
    console.log('message: ' + header);
    console.log(header);
  });
});
io.emit('some event', { someProperty: 'some value', otherProperty: 'other value' }); // This will emit the event to all connected sockets
io.on('connection',function(socket){
  //socket.broadcast.emit('hi');
});
io.on('connection', function(socket){
  socket.on('chat message',function(header){
    io.emit('chat message', header);
  });
});
http.listen(3000,function(){
  console.log('listening on *:3000');
});

/*
/*
io.on('connection',function(socket){
  socket.on('chat message',function(table){
    console.log(res.sendFile('/my.csv'));
  });
 }) */
/*
var $ = require('jquery'),
    csv = require('csv');

exports.index = function(server){

  var io = require('socket.io').listen(server);
  io.sockets.on('connection', function (socket) {

      socket.on('startTransmission', function(msg) {

        var timer=null, buffered=[], stream=csv().from.path('C:/dev/node_express/csv/test.csv', { delimiter: ',', escape: '"' });

        function transmit(row) {        
            socket.emit('transmitDataData', row);                                     
        }       

        function drain(timeout) {                                                    
            if (!timer) {
                timer = setTimeout(function() {                                    
                    timer = null;
                    if (buffered.length<=1) { // get more rows ahead of time so we don't run out. otherwise, we could skip a beat.
                        stream.resume(); // get more rows
                    } else {                        
                        var row = buffered.shift();
                        transmit(row);
                        drain(row[0]);                        
                    }

                }, timeout);               
            }                
        }

        stream.on('record', function(row,index){                        
            stream.pause();                                                                                   
            if (index == 0) {                            
                transmit(row);                                               
            } else {                            
                buffered.push(row);                                   
            }                                                       
            drain(row[0]); // assuming row[0] contains a timeout value.                                                                  
        });

        stream.on('end', function() {
            // no more rows. wait for buffer to empty, then cleanup.
        });

        stream.on('error', function() {
            // handle error.
        });

    });
};
 /*ss(socket).on('file', function(stream) {
  fs.createReadStream('/my.csv').pipe(stream);
});*/


/*
// Load requirements
var PORT = process.env.PORT || 8080;//just change port for other servers
var express = require("express");
var app = express();
var http = require("http").Server(app);
var io = require("socket.io")(http);

app.use(express.static(__dirname + '/public'));

var client = require('socket.io-client');

var socket1 = client.connect('http://localhost:8082', { reconnect: true });//connection to server B
var socket2 = client.connect('http://localhost:8080', { reconnect: true });//connection to server C

// Add a connect listener
io.on('connection', function (socket) {
    // socket variable name   ^^^^^^
    console.log('Client connected.');
//when server receive message from client
    socket.on('message_from_browser', function (message) {
        console.log("Message from browser broadcasted: " + message.text);
        var updated_message = {
           text: message.text,
           port: PORT  
        };

        // send message to server B
        socket1.emit('server_message', updated_message);//send message to server B
    });

    // Disconnect listener
    socket.on('disconnect', function () {
        console.log('Client disconnected.');
    });
});

io.on('connect', function(socket1){
    socket1.on('server_message', function (message) {
        console.log('RECEIVED MESSAGE FROM ANOTHER SERVER ON PORT '+ message.port + ": " + message.text);
    });
});

io.on('connect', function(socket2){
    socket2.on('server_message', function (message) {
        console.log('RECEIVED MESSAGE FROM ANOTHER SERVER ON PORT '+ message.port + ": " + message.text);
    });
});
http.listen(PORT, function (req, res) {
    console.log("Server Started on port: " + PORT);
});*/