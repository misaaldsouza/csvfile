/* Or use this example tcp client written in node.js.  (Originated with 
example code from 
http://www.hacksparrow.com/tcp-socket-programming-in-node-js.html.) */

var net = require('net');
var fs = require('fs');
var csv = require('fast-csv');

var client = new net.Socket();
client.connect(1337, '127.0.0.1', function() {
	console.log('Connected');
	//console.log('Hello, server! Love, Client.');
	
	client.write('Hello, server! Love, Client.');
	fs.createReadStream('my.csv')
   //.pipe(csv())
   .on('data',function(data){
    console.log(data.toString());
   })
   .on('end',function(data){
    console.log('Read finished');
   });
});

client.on('data', function(data) {
	console.log('Received: ' + data);
	//client.destroy(); // kill client after server's response
});

client.on('close', function() {
	console.log('Connection closed');
});